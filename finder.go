package main

import (
	"fmt"
	"log"
	"os"
	"runtime/debug"
	"syscall"
	"unsafe"

	"github.com/golang/geo/r2"
	"golang.org/x/sys/windows"
)

var GIT_VER = "Uncontrolled"

//go:generate go run github.com/akavel/rsrc -ico finder.ico -arch=amd64 -o icon.syso

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("panic\n%v\n%s\n\n", r, debug.Stack())
			fmt.Printf("failed, enter to exit\n")
		} else {
			fmt.Printf("success, enter to exit\n")
		}
		// print("Success\n")
		// time.Sleep(time.Second * 3)
		var b [10]byte
		os.Stdin.Read(b[:])
	}()

	fmt.Println("Version:", GIT_VER)

	var mons []rect

	success, _, errno := syscall.Syscall6(enumDisplayMonitors, 4, 0, 0, syscall.NewCallback(func(mon, dc uintptr, r *rect, m *[]rect) uintptr {
		// log.Println(r)
		*m = append(*m, *r)
		return 1
	}), uintptr(unsafe.Pointer(&mons)), 0, 0)
	if success == 0 {
		panic("failed EnumDisplayMonitors, err: " + errno.Error())
	}

	// log.Println(mons)

	var winds []window

	success, _, errno = syscall.Syscall(enumWindows, 2, syscall.NewCallback(func(hwnd uintptr, w *[]window) uintptr {
		u16str := make([]uint16, maxStrSize)
		var r rect
		var info windowinfo
		info.Cb = uint32(unsafe.Sizeof(info))
		success, _, errno := syscall.Syscall(getWindowTextW, 3, uintptr(hwnd), uintptr(unsafe.Pointer(&u16str[0])), maxStrSize)
		if success == 0 {
			if errno != 0 {
				log.Println("failed GetWindowTextW, err:", errno.Error())
			}
		}
		success, _, errno = syscall.Syscall(getWindowRect, 2, uintptr(hwnd), uintptr(unsafe.Pointer(&r)), 0)
		if success == 0 {
			log.Println("failed GetWindowRect, err:", errno.Error())
		}
		success, _, errno = syscall.Syscall(getWindowInfo, 2, uintptr(hwnd), uintptr(unsafe.Pointer(&info)), 0)
		if success == 0 {
			log.Println("failed GetWindowInfo, err:", errno.Error())
		}
		// log.Printf("%016x, %s, %v", hwnd, windows.UTF16ToString(u16str), info)
		*w = append(*w, window{Name: windows.UTF16ToString(u16str), Hwnd: hwnd, Rect: r})
		return 1
	}), uintptr(unsafe.Pointer(&winds)), 0)
	if success == 0 {
		panic("failed EnumWindows, err: " + errno.Error())
	}

	// log.Println(winds)

	// log.Println(r2.RectFromPoints(r2.Point{0, 0}, r2.Point{10, 10}))
	// log.Println(r2.RectFromPoints(r2.Point{0, 0}, r2.Point{10, 10}).Intersects(r2.RectFromPoints(r2.Point{10, 10}, r2.Point{11, 11})))

	for _, w := range winds {
		intersect := false
		for _, m := range mons {
			rwind := r2.RectFromPoints(r2.Point{X: float64(w.Rect.Left), Y: float64(w.Rect.Top)}, r2.Point{X: float64(w.Rect.Right) - 1, Y: float64(w.Rect.Bottom) - 1})
			mwind := r2.RectFromPoints(r2.Point{X: float64(m.Left), Y: float64(m.Top)}, r2.Point{X: float64(m.Right) - 1, Y: float64(m.Bottom) - 1})
			intersect = intersect || rwind.Intersects(mwind)
		}
		if !intersect {
			fmt.Print(w, "...")
			success, _, errno := syscall.Syscall9(setWindowPos, 7, w.Hwnd, ^uintptr(0)-(2-1), 0, 0, 0, 0, SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOZORDER, 0, 0)
			if success == 0 {
				fmt.Print("failed SetWindowPos, err:", errno.Error())
			} else {
				fmt.Print("done")
			}
			fmt.Print("\n")
		}
	}

	return
}

const maxStrSize = 256

type rect struct {
	Left, Top, Right, Bottom int32
}

type window struct {
	Name string
	Rect rect
	Hwnd uintptr
}

type windowinfo struct {
	Cb        uint32
	Window    rect
	Client    rect
	Style     uint32
	ExStyle   uint32
	Status    uint32
	XBorders  uint32 // go's uint has size 8 on windows/amd64, but windows' UINT has size 4
	YBoarders uint32 // go's uint has size 8 on windows/amd64, but windows' UINT has size 4
	Type      uint16
	Version   uint16
}

var (
	user32, _              = syscall.LoadLibrary("user32.dll")
	enumDisplayMonitors, _ = syscall.GetProcAddress(user32, "EnumDisplayMonitors")
	enumWindows, _         = syscall.GetProcAddress(user32, "EnumWindows")
	getWindowTextW, _      = syscall.GetProcAddress(user32, "GetWindowTextW")
	getWindowRect, _       = syscall.GetProcAddress(user32, "GetWindowRect")
	getWindowInfo, _       = syscall.GetProcAddress(user32, "GetWindowInfo")
	setWindowPos, _        = syscall.GetProcAddress(user32, "SetWindowPos")
)

const (
	SWP_NOSIZE     = 0x0001
	SWP_NOZORDER   = 0x0004
	SWP_NOACTIVATE = 0x0010
)
