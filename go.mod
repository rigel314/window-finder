module gitlab.com/rigel314/window-finder

go 1.14

require (
	github.com/golang/geo v0.0.0-20200730024412-e86565bf3f35
	golang.org/x/sys v0.0.0-20200803210538-64077c9b5642
)
