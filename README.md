window-finder
======

## About
This is a windows utility that moves any window that doesn't intersect the available monitors to 0,0 on the main monitor.

## Usage
Grab the latest release from [here](https://rigel314.gitlab.io/window-finder/window-finder.exe) or any release from [artifacts](https://gitlab.com/rigel314/window-finder/-/pipelines?scope=tags&page=1).

Then just double click the exe file.  You may have to tell Windows SafeScreen that the exe is allowed to run.  Then it should inform you of the windows it's moving, some default windows will fail.
