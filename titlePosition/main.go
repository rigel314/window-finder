package main

import (
	"flag"
	"log"
	"syscall"
	"unsafe"

	"golang.org/x/sys/windows"
)

var name = flag.String("name", "", "name of window to reposition")
var x = flag.Int("x", 0, "x pos")
var y = flag.Int("y", 0, "y pos")

func main() {
	flag.Parse()

	if *name == "" {
		log.Fatal("no name?")
	}

	var w uintptr
	syscall.Syscall(enumWindows, 2, syscall.NewCallback(func(hwnd uintptr, w *uintptr) uintptr {
		u16str := make([]uint16, maxStrSize)
		syscall.Syscall(getWindowTextW, 3, uintptr(hwnd), uintptr(unsafe.Pointer(&u16str[0])), maxStrSize)
		// log.Println(*name)
		thisname := windows.UTF16ToString(u16str)
		if *w != 0 && thisname == *name {
			*w = 0
			return 0
		}
		if thisname == *name {
			*w = hwnd
		}
		return 1
	}), uintptr(unsafe.Pointer(&w)), 0)

	if w == 0 {
		log.Fatal("window title not found or multiple matches found")
	}

	syscall.Syscall9(setWindowPos, 7, w, ^uintptr(0)-(2-1), uintptr(*x), uintptr(*y), 0, 0, SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOZORDER, 0, 0)
}

const maxStrSize = 256

var (
	user32, _         = syscall.LoadLibrary("user32.dll")
	enumWindows, _    = syscall.GetProcAddress(user32, "EnumWindows")
	getWindowTextW, _ = syscall.GetProcAddress(user32, "GetWindowTextW")
	setWindowPos, _   = syscall.GetProcAddress(user32, "SetWindowPos")
)

const (
	SWP_NOACTIVATE = 0x0010
	SWP_NOSIZE     = 0x0001
	SWP_NOZORDER   = 0x0004
)
